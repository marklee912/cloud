
var mysql = require('mysql');
var config = require('./config.json');


var db = {

  getConnection: function () {
    var connection = mysql.createConnection({
      host: config.database.host,
      port: config.database.port,
      user: config.database.user,
      password: config.database.pass,
      database: config.database.schema
    });

    return connection;
  },

  query: function (sql, valList, callback) {
    var con = this.getConnection();

    con.connect(function (err) {
      if (err) throw err;
      console.log("Connected!");

      con.query(sql, valList, callback);
      con.end();

    });
  }

};

module.exports = db;
