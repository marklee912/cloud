var AWS = require('aws-sdk');
var config = require('../config.json')

function DynamoDB(params, callback) {

    AWS.config.update(config.aws);

    // Create DynamoDB document client
    var documentClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });

    switch (params.Method) {
        case 'scan':
            documentClient.scan(params, function (err, data) {
                if (err) console.log(err);
                else callback(data.Items);
            });
            break;
        case 'query':
            documentClient.query(params, function (err, data) {
                if (err) console.log(err);
                else callback(data.Items);
            });
            break;
        case 'get':
            documentClient.get(params, function (err, data) {
                if (err) console.log(err);
                else callback(data.Item);
            });
            break;
        case 'put':
            documentClient.put(params, function (err, data) {
                if (err) console.log(err);
                else console.log(data);
            });
            break;
        default:
            console.log('Invalid dynamo method!');
            break;
    }
}

module.exports = DynamoDB;
