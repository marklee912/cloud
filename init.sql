


DROP TABLE IF EXISTS participation;
DROP TABLE IF EXISTS respondant;
DROP TABLE IF EXISTS service;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS department;


CREATE TABLE IF NOT EXISTS department (
  id INT AUTO_INCREMENT,
  name VARCHAR(128) NOT NULL UNIQUE,
  service_type  INT NOT NULL,
  address VARCHAR(128) NOT NULL,
  hotline   VARCHAR(32) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS user (
  id INT AUTO_INCREMENT,
  fname VARCHAR(64),
  lname VARCHAR(64),
  gender  VARCHAR(16),
  age  INT NOT NULL,
  address VARCHAR(128) NOT NULL,
  phone   VARCHAR(16) NOT NULL,
  email  VARCHAR(128) NOT NULL,
  health  VARCHAR(1024) DEFAULT 'healthy',
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS service (
  id VARCHAR(64) NOT NULL ,
  user_id INT NOT NULL ,
  service_type INT NOT NULL ,
  department_id INT NOT NULL ,
  status  INT NOT NULL,
  request_time DATETIME DEFAULT CURRENT_TIMESTAMP,
  start_time DATETIME,
  end_time   DATETIME,
  detail  VARCHAR(1024) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE,
  FOREIGN KEY (department_id) REFERENCES department(id) ON DELETE CASCADE

);


CREATE TABLE IF NOT EXISTS respondant (
  id INT AUTO_INCREMENT,
  department_id INT NOT NULL ,
  fname VARCHAR(64),
  lname VARCHAR(64),
  age INT NOT NULL,
  title VARCHAR(128),
  status  INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (department_id) REFERENCES department(id) ON DELETE CASCADE

);

CREATE TABLE IF NOT EXISTS participation (
  id INT AUTO_INCREMENT,
  service_id VARCHAR(64) NOT NULL ,
  respondant_id INT NOT NULL ,
  PRIMARY KEY (id),
  FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE,
  FOREIGN KEY (respondant_id) REFERENCES respondant (id) ON DELETE CASCADE

);

CREATE UNIQUE INDEX part_id ON participation (service_id, respondant_id);

INSERT INTO user (fname, lname, gender, age, address, phone, email, health)
VALUES ("Mona","Oliver","female",48,"P.O. Box 288, 9462 Sed, St.","64-021-025-26136","condimentum.Donec@Sed.net","healthy"),
    ("Mary","Alvarado","female",38,"655 A, St.","64-021-022-26157","purus.in@sit.org","healthy"),
    ("Sophie","Spence","female",44,"Ap #790-8627 Sed Ave","64-021-023-95582","laoreet.libero@maurisblandit.edu","healthy"),
    ("Ben","Gibson","male",24,"Ap #551-5078 Ad Road","64-021-024-64690","id.erat@aarcuSed.com","healthy"),
    ("Timmy","Spence","male",48,"484-3416 Amet Road","64-021-020-98031","ut.nulla.Cras@etrutrumeu.org","healthy"),
    ("John","Bush","male",16,"P.O. Box 876, 5281 Scelerisque Rd.","64-021-023-25989","varius.orci.in@tempusrisusDonec.org","healthy");


INSERT INTO department (name, service_type, address, hotline)
VALUES ('Hamilton Central Police Station', 1, '12 Anzac Parade', '(07) 858 6200'),
  ('Cambridge Police Station', 1, '18 Dick St', '(07) 827 5531'),
  ('Ngaruawahia Police Station',  1, '12 Waikato Esplanade', '(07) 824 8199'),
  ('Hamilton Fire Station', 0, '189 Anglesea Street', '07 839 4996'),
  ('Pukete Fire Station', 0, '63 Vickery Street', '07 849 0373'),
  ('Chartwell Fire Station', 0, '70 Crosby Road', '07 854 7941'),
  ('St John Ambulance Station', 2, '63 Seddon Rd, Frankton, Hamilton 3204', '07 847 2849'),
  ('Ambulance EMT', 2, 'Whatawhata Rd, Dinsdale, Hamilton 3204', '0800 367 368');




INSERT INTO service (id, user_id, service_type, department_id, status, request_time, start_time, end_time, detail)
VALUES ('bb18dc4b-fd06-413b-9ddd-0ccd37d6bd46', 1, 0, 1, 2, '2017-07-21 13:20:15', '2017-07-21 13:23:15','2017-07-21 13:34:15', 'robbery' )
      ,('c55e6588-b12b-4b69-898a-e69e766e5c63', 1, 0, 1, 2, '2018-01-11 13:20:15', '2018-01-11 13:23:15','2018-01-11 13:34:15', 'robbery');

INSERT INTO respondant (department_id, fname, lname, age, title, status)
VALUES (1, 'James', 'Smith', 30, 'Senior Police Officer', 0 )
  ,(2,'Henry', 'Cook', 36, 'Police Officer', 0 ),
  (3, 'Daniel','Hobbs', 25, 'Police Officer', 0),
  (4, 'Stone', 'Odom', 28, 'Firefighter Leader', 0),
  (5, 'Lucas', 'Weaver', 30, 'Firefighter Leader', 0),
  (6, 'Garrison', 'Wall', 26, 'Deputy Firefighter Leader', 0),
  (7, 'Rose', 'Valdez', 25, 'Ambulance Officer', 0),
  (8, 'Kimberly', 'Spence', 27, 'Ambulance Officer', 0);


INSERT INTO participation(service_id, respondant_id)
VALUES ('bb18dc4b-fd06-413b-9ddd-0ccd37d6bd46', 1);