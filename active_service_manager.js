var dynamo = require('./aws/dynamo')
var uuid = require('uuid-v4');

function ActiveService(serviceId, caller, respondents, sio, myManager) {

    this.manager = myManager;
    this.id = serviceId;
    this.respondants = respondents;
    this.caller = caller;
    this.getNspId = () => ('/task-' + this.id);

    console.log('nsp created ' + this.getNspId());
    var nsp = sio.of(this.getNspId());
    this.nsp = nsp;
    nsp.on('connection', function (socket) {

        // read from dynamo
        dynamo({
            Method: 'query',
            TableName: 'Message',
            IndexName: 'serviceId-index',
            KeyConditionExpression: '#k = :v',
            ExpressionAttributeNames: {
                '#k': 'serviceId'
            },
            ExpressionAttributeValues: {
                ':v': this.id,
            }
        }, res => {
            console.log(res);
            // deal with the query results here

        })

        //this.client_socks.push(socket);
        nsp.emit('msg', { user: 'admin', text: 'Hi, every one' });
        socket.on('msg', function (data) {
            console.log('message: ' + data);

            // save to dynamo
            dynamo({
                Method: 'put',
                TableName: 'Message',
                Item: {
                    id: uuid(),
                    serviceId: this.id,
                    time: new Date().getTime(),
                    data: data,
                }
            })

            nsp.emit('msg', data);
        });

        socket.on('loc', function(data){
            if (data.role === 'caller') {
                manager.updateCallerLocation(data.sid, data.id, data.lat, data.long);
            } else if (data.role === 'responder') {
                manager.updateRespondantLation(data.sid, data.id, data.lat, data.long);
            }
            console.log('broadcast loc:' + JSON.stringify(data));
            nsp.emit('loc', data);
        });
    });

    this.release = function (sio) {
        this.nsp.removeAllListeners();
        delete sio.nsps[this.getNspId()];
    },

    this.toData = function () {

            var data = {
                id: this.id,
                caller: this.caller,
            };

            var arr = [];
            this.respondants.forEach((val, key) => {
                var d = {};
                d.id = val.id;
                d.lat = val.lat;
                d.long = val.long;
                arr.push(d);
            });

            data.respondants = arr;


            return data;
        };

    this.setManager = function (m) {
        this.manager = m;
    }

    this.getManager = function () {
        return this.manager;
    }
}


var manager = {

    //item {id: 123, latitude: -37.00, longitude: 175.000}
    callerLocationMap: new Map(),

    //item {id: 123, latitude: -37.00, longitude: 175.000}
    respondentLocationMap: new Map(),

    //item {id: 'sdfsdfk-sd-e34sdf-2d', caller: c1, respondents: {}}
    activeServices: new Map(),

    operatorNsp: null,

    hasOperatorNsp: function() {
        if (this.operatorNsp) {
            return true;
        } else {
            return false;
        }
    },

    createOperatorNsp: function(sio) {

        console.log('create /operator channel!');
        this.operatorNsp = sio.of('/operator');

        this.operatorNsp.on('connection', function(socket) {

            // console
            manager.pushAllLocation();


        });
    },

    getActiveService: function(serviceId) {
        return this.activeServices.get(serviceId);
    },

    createActiveService: function (serviceId, callerId, respondentIds, sio) {

        if (!this.hasOperatorNsp()) {
            this.createOperatorNsp(sio);
        };

        var respondants = new Map();
        var caller = {id : callerId};
        caller.lat = 999.99;
        caller.long = 999.99;
        this.callerLocationMap.set(callerId, caller);
        for (var i = 0; i < respondentIds.length; i++) {
            var respondant = {id: respondentIds[i]};
            respondant.lat = 999.99;
            respondant.long = 999.99;
            this.respondentLocationMap.set(respondentIds[i], respondant);
            respondants.set(respondentIds[i], respondant);
        }

        var active = new ActiveService(serviceId, caller, respondants, sio, this);

        this.activeServices.set(serviceId, active);

        return active;
    },

    deleteActiveService: function(serviceId, sio) {
        var active = this.activeServices.get(serviceId);
        active.release(sio);
        this.activeServices.delete(serviceId);
    },

    getActiveServicesData: function () {

        var data = [];
        this.activeServices.forEach((value, key) => {
            // do stuff
            data.push(value.toData());
        });

        return data;
    },

    hasActiveService : function (serviceId){
        return this.activeServices.has(serviceId);
    },

    updateCallerLocation: function (sid, id, lat, long) {
        var caller = this.callerLocationMap.get(id);
        caller.lat = lat;
        caller.long = long;

        var data = {};

        data.sid = sid;
        data.role = 'caller';
        data.id = caller.id;
        data.lat = caller.lat;
        data.long = caller.long;

        this.operatorNsp.emit('loc', data);

    },

    updateRespondantLation: function (sid, id, lat, long) {
        var responder = this.respondentLocationMap.get(id);
        responder.lat = lat;
        responder.long = long;

        var data = {};

        data.sid = sid;
        data.role = 'responder';
        data.id = responder.id;
        data.lat = responder.lat;
        data.long = responder.long;

        this.operatorNsp.emit('loc', data);
    },

    pushAllLocation: function() {

        if (this.hasOperatorNsp()) {

            this.activeServices.forEach((active, sid) => {

                var caller = active.caller;
                var respdonder = active.respondants.values().next();
                var data = {};

                data.sid = sid;
                data.role = 'caller';
                data.id = caller.id;
                data.lat = caller.lat;
                data.long = caller.long;


                this.operatorNsp.emit('loc', data);

                data.role = 'responder';
                data.id = respdonder.id;
                data.lat = respdonder.lat;
                data.long = respdonder.long;
                this.operatorNsp.emit('loc', data);

            });

        };

    },

};

module.exports = manager;
