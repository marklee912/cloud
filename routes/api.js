

var express = require('express');
var db = require('../db');
var uuid = require('uuid-v4');
var moment = require('moment');
var sessionManager = require('../active_service_manager');

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set region
AWS.config.update({region: 'ap-southeast-2'});

var router = express.Router();

var sio = null;

router.setSocketIO = (io) => {
    sio = io;
}

router.getSocketIO = () => {
    return sio;
}


router.hasActiveService = (serviceId) => {
    return sessionManager.hasActiveService(serviceId);
}

// operation { get: func; post:func; put:func, delete:func}

var dummyBeforeOperation = {
    get: (req) => {},
    post: (req) => {},
    put: (req) => {},
    delete: (req) => {}
};


var dummyAfterOperation = {
    get: (req, result) => {},
    post: (req, result) => {},
    put: (req, result) => {},
    delete: (req, result) => {}
};

function registerTableApiRoute(router, tablePath, tableName, beforeQuery, afterQuery) {

    // url ['/tablePath']
    router.get(tablePath, function (req, res, next) {

        if (beforeQuery.get != undefined)
            beforeQuery.get(req);

        db.query('SELECT * FROM ' + tableName , function (err, result) {
            console.log(result);

            if (afterQuery.get !== undefined)
                afterQuery.get(req, result);
            res.send(JSON.stringify(result));
        });

        // var d = { 'id': 1, 'name': 'Hamilton Police Station' };
        // res.send(JSON.stringify(d));

    });

    router.post(tablePath, function (req, res) {

        if (beforeQuery.post !== undefined)
            beforeQuery.post(req);

        db.query('INSERT INTO '+ tableName + ' SET ?', req.body, function(err, result) {
            if (err) throw err;

            if (afterQuery.post !== undefined)
                afterQuery.post(req, result);
            res.end(JSON.stringify(result));

        });
    });

    router.route(tablePath + '/:id')
    .get(function (req, res) {

        if (beforeQuery.get !== undefined)
            beforeQuery.get(req);
    	db.query('SELECT * FROM ' + tableName + ' WHERE id=?', [req.params.id], function (error, results) {
            if (error) throw error;
            
            if (afterQuery.get !== undefined)
                afterQuery.get(req, result);

    		res.end(JSON.stringify(results));
    	});
    }).put(function (req, res) {
        console.log(req.body);
        if (beforeQuery.put !== undefined)
            beforeQuery.put(req);
    	db.query('UPDATE ' + tableName + ' SET ? WHERE id=?', [req.body, req.params.id], function(error, results){
            if (error) throw error;

            if (afterQuery.put !== undefined)
                afterQuery.put(req, result);

    		res.end(JSON.stringify(results));
    	});
    }).delete(function (req, res) {
        console.log('delete ' + tableName + req.params.id);

        if (beforeQuery.delete !== undefined)
            beforeQuery.delete(req);

    	db.query('DELETE FROM ' + tableName + ' WHERE id=?', [req.params.id], (error, results) => {
            if (error) throw error;
            
            if (afterQuery.delete !== undefined)
                afterQuery.delete(req, result);

    		res.end( req.params.id + ' has been deleted! ' + JSON.stringify(result));
    	});
    });

}

registerTableApiRoute(router, '/department', 'department', {}, {});
registerTableApiRoute(router, '/user', 'user', {}, {});

var beforeCreateService = {
    post: (req) => { 
        req.body.id = uuid(); 
    
    }
};


registerTableApiRoute(router, '/service', 'service', beforeCreateService, {});
registerTableApiRoute(router, '/respondant', 'respondant',  {}, {});
registerTableApiRoute(router, '/participation', 'participation',  {}, {});

router.route('/activeservice')
.get(function(req, res){
    res.end(JSON.stringify(sessionManager.getActiveServicesData()));
}).post(function (req, res) {

    req.body.id = uuid();
    req.body.status = 1;

    console.log("request to create ActiveService for caller id = " + req.body.user_id);
    //req.body.request_time = moment.now();
    //req.body.start_time = moment.now();

    var respondantIds = Object.assign([], req.body.respondantIds);
    
    // respondantIds array can't pass to sql
    delete req.body.respondantIds;

    var serviceId = req.body.id;
    var callId = req.body.user_id;

    db.query('INSERT INTO service SET ?', req.body, function(err, result) {

        var params = [];
        for (var i = 0; i < respondantIds.length; i++) {
            params.push([serviceId, respondantIds[i]]);
            db.query('UPDATE respondant SET ? WHERE id=?', [ {status:1}, respondantIds[i]], (error, result) => {
                if (error) throw error;
            });
        }
        // create participation records
        db.query('INSERT INTO participation(service_id, respondant_id) VALUES ?', [params], function(err, results) {
            if (err) err;

            var active = sessionManager.createActiveService(serviceId, callId, respondantIds, sio);
            var text = JSON.stringify(active.toData());
            res.end(text);


            db.query('SELECT * from user WHERE id=?', [callId], function(err, result){
                if (err) err;

                var callerPhone = result[0].phone;
                var callerFname = result[0].fname;

                // Send sms to caller with URL
                var callerurl = 'http://localhost:3000/' + 'callersession' + serviceId + '?cid=' + callId; 
                var callerMessage = 'Hi ' + callerFname + ", please click on the link to share your location with us: " + callerurl;
                
                console.log('callerURL:' + callerurl, ', callerMsg: ' + callerMessage); 

                /*sendSNS( callerPhone, callerMessage
                    ,function(data) {
                    console.log("MessageID is " + data.MessageId);
                  });*/
            });

            db.query('SELECT * from respondant WHERE id=?', [respondantIds[0]], function(err, result){
                var resPhone = result[0].phone;
                var resFname = result[0].fname;

                // Send sms to responder with URL
                var resurl = 'http://localhost:3000/' + 'respondersession' + serviceId + '?cid='+ callId + '&rid='+ respondantIds[0]; 
                var resMessage = 'Hi ' + resFname + ", please click on the link to view the caller location: " + resurl;
                
                console.log('resURL:' + resurl, ', resMsg: ' + resMessage); 

                /*sendSNS( resPhone, resMessage
                    ,function(data) {
                    console.log("MessageID is " + data.MessageId);
                  });*/

            }); 
            
        });

    });
});
router.route('/activeservice/:id')
.get(function(req, res) {

    var serviceId = req.params.id;
    var active = sessionManager.getActiveService(serviceId);
    res.end(JSON.stringify(active.toData()));

}).delete(function(req, res) {
    console.log('delete activeService ' + req.params.id);

    var serviceId = req.params.id;
    var active = sessionManager.getActiveService(serviceId);
    if (active === undefined) {
        res.end("{}");
        return;
    }
    var respondants = active.respondants;

    sessionManager.deleteActiveService(serviceId, sio);

    var now = moment().format('YYYY-MM-DD hh:mm:ss');

    db.query('UPDATE service SET ? WHERE id=?', [ {end_time:now, status:2}, serviceId], (error, results) => {
        if (error) throw error;

        // set the status of all respondants to 0
        respondants.forEach((resp, key) => {
            console.log('set respondant ' + resp.id + ' status to 0' );
            db.query('UPDATE respondant SET ? WHERE id=?', [ {status:0}, resp.id], (err, result) => {
            if (error) throw error;
            });
        });
    
        res.end( req.params.id + ' has been deleted! ' + JSON.stringify(results));
    });
});

function loadActiveService() {

    var data = new Map();

    db.query(`SELECT respondant.id AS rid, service.id AS sid FROM respondant 
    INNER JOIN participation ON respondant.id=participation.respondant_id
    INNER JOIN service on service.id=participation.service_id
    WHERE service.status=1`, [], function(err, results) {

        let curSid = null;
        var respondantIds = [];

        for (var i = 0; i < results.length; i++) {
            var res = results[i];
            if (data.has(res.sid)) {
                curSid = res.sid;
                respondantIds.push(res.rid);

            } else {
                // new service
                if (curSid !== null) {
                    //console.log("curSid:" + curSid);
                    const sid = curSid;
                    db.query('SELECT * FROM service WHERE id=?', [sid], (err, result2) => {
                        var callId = result2[0].user_id;
                        sessionManager.createActiveService(sid, callId, data.get(curSid), sio);      
                    });
                }

                curSid = res.sid;
                ids = [];
                ids.push(res.rid);
                data.set(curSid, ids);
            }
        }

        const sid2 = curSid;
        db.query('SELECT * FROM service WHERE id=?', [curSid], (err, result2) => {
            if (err) throw err;
            if (result2.length > 0) {
                var callId = result2[0].user_id;
                sessionManager.createActiveService(sid2, callId, data.get(sid2), sio);               
            }
        });


    });
}

loadActiveService();

function sendSNS(userPhone, msg, callback){
    var params = {
        Message: msg,
        PhoneNumber: userPhone, //Change to userPhone
      };
      
      // Create promise and SNS service object
      var publishTextPromise = new AWS.SNS({apiVersion: '2010-03-31'}).publish(params).promise();
      
      // Handle promise's fulfilled/rejected states
      publishTextPromise.then(callback).catch(
        function(err) {
        console.error(err, err.stack);
      });

}

module.exports = router;