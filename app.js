var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var apiRouter = require('./routes/api');
var albumRounter = require('./routes/album')

// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set region
AWS.config.update({region: 'ap-southeast-2'});

var app = express();

// enable all CORS
app.use(cors());

app.setSocketIO = (sio) => {

  var nsp = sio.of('/sessions');
  this.nsp = nsp;
  nsp.on('connection', function(socket) {
  
      nsp.emit('msg', 'Hi, every one');

      socket.on('msg', function(msg){
        console.log('message: ' + msg);
        nsp.emit('msg', msg);
        //nsp.emit('msg', {lat:99, long:100});
      });

      socket.on('loc', function(data){
        nsp.emit('loc', data);
      });
   });

  apiRouter.setSocketIO(sio);
}

app.getSocketIO = () => {
    return apiRouter.getSocketIO();
}
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api', apiRouter);
app.use('/album', albumRounter);

// app.use('/caller/*', function(req, res){
//     res.sendfile('public/chat.html');
// });

const CALLER_URL_PATTERN = '/callersession';
const RESPONDER_URL_PATTERN = '/respondersession';


app.use(CALLER_URL_PATTERN + "*", function (req, res){
  var sid = req.baseUrl.substr(CALLER_URL_PATTERN.length);

  if (apiRouter.hasActiveService(sid)) {
      res.sendfile(__dirname + '/public/caller.html');
  } else {
      res.status = 404;
      res.send("No Such Service");
  }

});

app.use(RESPONDER_URL_PATTERN + "*", function (req, res){

  var sid = req.baseUrl.substr(RESPONDER_URL_PATTERN.length);

  if (apiRouter.hasActiveService(sid)) {
    res.sendfile(__dirname + '/public/responder.html');
  } else {
    res.status = 404;
    res.send("No Such Service");
  }

});


app.post('/admin', function(req, res) {
  if (req.body.username != undefined && req.body.username != undefined){
    res.sendfile(__dirname + '/public/admin.html');
  } else
  res.sendfile(__dirname + '/public/login.html');
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
