
const ServiceType_FireService = 0;
const ServiceType_Police = 1;
const ServiceType_Ambulance = 2;

const STATUS_NO_SERVICE = 0;
const STATUS_IN_SERVICE = 1;

const RESP_STAT_AVAILABLE = 0;
const RESP_STAT_ONDUTY = 1;
const RESP_STAT_ONLEAVE = 2;



var ServiceTypes = {};
ServiceTypes[ServiceType_FireService] = "Fire Service";
ServiceTypes[ServiceType_Police] = "Police";
ServiceTypes[ServiceType_Ambulance] = "Ambulance";


var gCallers = new Map();
var gDepartments = new Map();
var gRespondants = new Map();
var gServices = new Map();
var gActiveServices = new Map();


function loadAllData(callback) {
    $.get('/api/user', function(text) {

        var data = JSON.parse(text);
        for (var i = 0; i < data.length; i++) {
            var d = data[i];
            d.status = STATUS_NO_SERVICE;
            gCallers.set(d.id, d);
        }

        $.get('/api/department', function(text) {
            //alert(JSON.stringify(data));
            var data = JSON.parse(text);
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                gDepartments.set(d.id, d);
            }
            $.get('/api/respondant', function(text) {

                var data = JSON.parse(text);
                for (var i = 0; i < data.length; i++) {
                    var d = data[i];
                    d.department = gDepartments.get(d.department_id);
                    gRespondants.set(d.id, d);
                }
    
                $.get('/api/service', function(text){
    
                    var data = JSON.parse(text);
                    for (var i = 0; i < data.length; i++) {
                        var d = data[i];
                        addService(d);
                    }
    
    
                    $.get('/api/activeservice', function(text){
    
                        var data = JSON.parse(text);
                        for (var i = 0; i < data.length; i++) {
                            var d = data[i];

                            addActiveService(d);
                        }
    
                        callback();
                    });
                    
                });        
            });
        });
    
    });


    console.log(gDepartments);

}


function addService(service) {

    gServices.set(service.id, service);
    service.caller = gCallers.get(service.id);
    service.department = gDepartments.get(service.department_id);

    return service;
}

function addActiveService(d) {
    gActiveServices.set(d.id, d);
    d.service = gServices.get(d.id);
    var caller = gCallers.get(parseInt(d.caller.id));
    d.caller = Object.assign(caller, d.caller);


    var resps = [];
    for (var j = 0; j < d.respondants.length; j++) {
        var r = d.respondants[j];
        var p = gRespondants.get(parseInt(r.id));
        p.lat = r.lat;
        p.long = r.long;
        resps.push(p);
    }
    d.respondants = resps;
    d.responder = resps[0];

    d.caller.lat = d.caller.lat;
    d.caller.long = d.caller.long;
    d.caller.status = STATUS_IN_SERVICE;
}


function updateNewActiveService(service, activeData) {

    


    var d = active;
    gActiveServices.set(d.id, d);
    d.service = gServices.get(d.id);
    var caller = gCallers.get(d.caller.id);
    d.caller = Object.assign(caller, d.caller);



    var resps = [];
    for (var j = 0; j < d.respondants.length; j++) {
        var r = d.respondants[j];
        var p = gRespondants.get(r.id);
        p.lat = r.lat;
        p.long = r.long;
        resps.push(p);
    }
    d.respondants = resps;

    d.caller.lat = d.caller.lat;
    d.caller.long = d.caller.long;
    d.caller.status = STATUS_IN_SERVICE;
}







/* {callerId, service_type, departid, respondantIds }*/
function requestCreateService(data, callback) {

    $.post('/api/activeservice', data, function(result){
        callback(result);
    });

}

function testCreateService() {

                var data = {};
                data.user_id = 1;
                data.service_type = 0;
                data.department_id = 1;
                data.respondantIds = [1];
                // auto data.status 
                // auto data.id
                // data.request_time
                // data.start_time
                data.detail = 'breakin';

                requestCreateService(data, function(result) {

                });
}


// $(function(){
//     loadAllData(
//         //testCreateService()
//         () => {}
//         // function () {
//         //     console.log("xxx, xxx");
//         // }
//     );

// });