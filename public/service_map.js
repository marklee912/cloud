

var LEVEL = 15;
var DEFAULT_LONGITUDE = 175.00;
var DEFAULT_LATITUDE = -37.788;

//var xyz = 'http://ec2-13-55-9-72.ap-southeast-2.compute.amazonaws.com:8080/styles/klokantech-basic/{z}/{x}/{y}.png';

var xyz = 'https://sysdmrobin.tk/map/styles/klokantech-basic/{z}/{x}/{y}.png';


//var xyz = 'http://localhost:8080/styles/klokantech-basic/{z}/{x}/{y}.png';


function Respondant(obj) {
    this.id = obj.id;
    this.title = obj.title;
    this.fname = obj.fname;
    this.lname = obj.lname;

    this.latitude = obj.lat;
    this.longitude = obj.long;

    this.circle = null;

    this.position = function () {
        return [this.latitude, this.longitude];
    };

    this.hasPosition = function() {
        return isLatitudeValid(this.latitude) && isLongitudeValid(this.longitude);
    };

    this.show = function (map) {
        if (this.circle === null && this.hasPosition()) {
            this.circle = L.circle([this.latitude, this.longitude], {radius: 50}).addTo(map);
        }
    };

    this.hide = function (map) {
        if (this.circle !== null) {
            map.removeLayer(this.circle);
            this.circle = null;
        }
    };

    this.updatePosition = function(map, lat, long) {
        this.latitude = lat;
        this.longitude = long;
        var active = this.getService();
        active.update(map);
    };

    this.setService = function (service) {
        this.service = service;
    };

    this.getService = function () {
        return this.service;
    };
}

function isLatitudeValid(lat) {
    return (lat <= 90.0) && (lat >= -90.0);
}

function isLongitudeValid(long) {
    return (long <= 180.0) && (long >= -180.0);
}

function Caller(obj) {
    this.id = obj.id;
    this.fname = obj.fname;
    this.lname = obj.lname;
    this.latitude = obj.lat;
    this.longitude = obj.long;

    this.circle = null;

    this.show = function (map) {
        if (this.circle === null && this.hasPosition()) {
            var param = {
                color: 'red',
                fillColor: '#f03',
                fillOpacity: 0.5,
                radius: 50
            };
            this.circle = L.circle([this.latitude, this.longitude], param).addTo(map);
        }
    };

    this.hide = function (map) {
        if (this.circle !== null) {
            map.removeLayer(this.circle);
            this.circle = null;
        }
    };

    this.hasPosition = function() {
        return isLatitudeValid(this.latitude) && isLongitudeValid(this.longitude);
    },

    this.position = function () {
        return [this.latitude, this.longitude];
    };

    this.updatePosition = function(map, lat, long) {
        this.latitude = lat;
        this.longitude = long;
        var active = this.getService();
        active.update(map);
    };

    this.setService = function (service) {
        this.service = service;
    }

    this.getService = function () {
        return this.service;
    }
}

function Service(obj) {
    this.id = obj.id;
    this.service_type = obj.service_type;

    this.caller = new Caller(obj.caller);
    this.caller.setService(this);

    this.respondant = new Respondant(obj.responder);
    this.respondant.setService(this);

    this.line = null;

    this.respPosition = function () {
        return this.respondant.position();
    };

    this.callerPosition = function () {
        return this.caller.position();
    };

    this.show = function (map) {

        if (this.line === null && this.caller.hasPosition() && this.respondant.hasPosition()) {
            this.line = L.polyline([this.respPosition(), this.callerPosition()]).addTo(map);
        }

        this.caller.show(map);
        this.respondant.show(map);
    };

    this.hide = function (map) {
        if (this.line !== null) {
            map.removeLayer(this.line);
            this.line = null;
        }

        this.caller.hide(map);
        this.respondant.hide(map);
    };

    this.update = function(map) {
        this.hide(map);
        this.show(map);
    }

}


function ServiceMap(latitude, longitude) {

    this.map = L.map('map', {center: [latitude, longitude], zoom: LEVEL});
    this.services = new Map();
    this.center_lat = latitude;
    this.center_long = longitude;

    this.showServices = function() {
        //this.map.setView(this.center_lat, this.center_long, LEVEL);

        L.tileLayer(xyz, {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            //accessToken: 'your.mapbox.access.token'
        }).addTo(this.map);

        L.marker([this.center_lat, this.center_long]).addTo(this.map)
            .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
            .openPopup();
        
        this.services.forEach(function(service, id){
            service.show(this.map);
        });
    };

    this.removeService = function ( serviceId ) {
        var service = this.services.get(serviceId);
        service.hide(this.map);
        this.services.delete(serviceId);
    };

    this.addService = function (obj) {
        var service = new Service(obj);
        this.services.set(service.id, service);
        service.show(this.map);
    };

    this.updatePosition = function(sid, role, lat, long) {
        var active = this.services.get(sid);
        if (role === 'caller') {
            active.caller.updatePosition(this.map, lat, long);
        } else if (role === 'responder') {
            active.respondant.updatePosition(this.map, lat, long);
        }
    };

    this.showServices();

}