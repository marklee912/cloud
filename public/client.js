



function initMap(callback) {
    var map = null;
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            map = new ServiceMap(position.coords.latitude, position.coords.longitude, LEVEL);
            callback(map);
            //map.addService(active);
        });
    } else {
        //x.innerHTML = "Geolocation is not supported by this browser.";
        map = new ServiceMap(DEFAULT_LATITUDE, DEFAULT_LONGITUDE, LEVEL);
        callback(map);
        //map.addService(active);
    }
}



function loadService(sessionId, callid, role, callback) {


    $.get('/api/user/' + callid, function (text) {

        var caller = JSON.parse(text)[0];

        $.get('/api/activeservice/' + sessionId, function (text) {
            var d = JSON.parse(text);
            caller.lat = d.caller.lat;
            caller.long = d.caller.long;

            var r = d.respondants[0];

            $.get('/api/respondant/' + r.id, function(text2) {
                var data2 = JSON.parse(text2);
                var responder = data2[0];
                responder.lat = r.lat;
                responder.long = r.long;

                $.get('/api/service/' + sessionId, function(text3){
                    var data3 = JSON.parse(text3);
                    var service = data3[0];

                    service.caller = caller;
                    service.responder = responder;

                    var person = null;
                    if (role === 'caller') {
                        person = caller;
                    } else if (role == 'responder') {
                        person = responder;
                    }

                    document.getElementById('chat').contentWindow.connectChatRoom(sessionId, person, role);
                    callback(service);
                });


            }) 

        });


    })

}